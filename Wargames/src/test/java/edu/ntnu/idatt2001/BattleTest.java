package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.units.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class BattleTest
 * @author Edvard Schøyen
 *
 */
class BattleTest {

    @Test
    void simulate() {
        Army testOne = new Army("Test");
        Army testTwo = new Army("Test");
        for (int i = 0; i <10 ; i++) {
            testOne.add(new InfantryUnit("test",10));
            testTwo.add(new InfantryUnit("test",10));
        }
        for (int i = 0; i <10 ; i++) {
            testOne.add(new RangedUnit("test",10));
            testTwo.add(new RangedUnit("test",10));
        }
        for (int i = 0; i <10 ; i++) {
            testOne.add(new CavalryUnit("test",10));
            testTwo.add(new CavalryUnit("test",10));
        }
        testOne.add(new CommanderUnit("test",10));
        testTwo.add(new CommanderUnit("test",10));
        Battle battle = new Battle(testOne,testTwo);
        battle.simulate();
    }

    @Test
    void testToString() {
    }

    @Test
    void checkWhoWon() {
        Army testOne = new Army("Test");
        Army testTwo = new Army("Test");
        testOne.add(new InfantryUnit("win",10));
        //assertEquals("",Battle.checkWhoWon(testOne,testTwo));
    }

    @Test
    void battleStats() {
        /*
        Army armyOne = new Army("Test1");
        Army armyTwo = new Army("Test2");
        armyOne.add(new InfantryUnit("Test1",1));
        armyTwo.add(new InfantryUnit("Test2",100));
        Battle battleTest = new Battle(armyOne, armyTwo);
        battleTest.simulate();
        if (armyOne.hasUnits()){
            assertTrue(armyOne.hasUnits());
        }else {
            assertTrue(armyTwo.hasUnits());
        }
        */
        battleStats();
    }

    @Test
    void isDead() {
        /*
        Army armyOne = new Army("Test1");
        Army armyTwo = new Army("Test2");
        armyOne.add(new InfantryUnit("Test1",1));
        armyTwo.add(new InfantryUnit("Test2",100));
        Battle battleTest = new Battle(armyOne, armyTwo);
        battleTest.simulate();
        if (armyOne.hasUnits()){
            assertTrue(armyOne.hasUnits());
        }else {
            assertTrue(armyTwo.hasUnits());
        }
         */

        Unit test = new InfantryUnit("isdead",1);
        test.setHealth(-2);
        assertTrue(Battle.isDead(test));
    }

}