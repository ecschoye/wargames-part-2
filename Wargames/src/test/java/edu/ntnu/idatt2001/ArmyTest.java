package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.units.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class ArmyTest
 * @author Edvard Schøyen
 *
 */
class ArmyTest {

    @Test
    void getInfantryUnitsTest() {
        int i = 0;
        Army armyTest = new Army("TestArmy");
        for (i = 0; i<1; i++){
            armyTest.add(new InfantryUnit("Footman", 100));
        }
        for (i = 0; i<1; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        assertEquals(1,armyTest.getInfantryUnits().size());

    }

    @Test
    void getCavalryUnitsTest() {
        int i = 0;
        Army armyTest = new Army("TestArmy");
        for (i = 0; i<1; i++){
            armyTest.add(new InfantryUnit("Footman", 100));
        }
        for (i = 0; i<1; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        armyTest.add(new CommanderUnit("test",100));
        assertEquals(1,armyTest.getCavalryUnits().size());
        assertEquals(1,armyTest.getCommanderUnits().size());
    }

    @Test
    void getRangedUnitsTest() {
        int i = 0;
        Army armyTest = new Army("TestArmy");
        for (i = 0; i<1; i++){
            armyTest.add(new RangedUnit("Ranger", 100));
        }
        for (i = 0; i<1; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        assertEquals(1,armyTest.getRangedUnits().size());
    }

    @Test
    void getCommanderUnitsTest() {
        int i = 0;
        Army armyTest = new Army("TestArmy");
        for (i = 0; i<=10; i++){
            armyTest.add(new InfantryUnit("Footman", 100));
        }
        for (i = 0; i<=10; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        for (i = 0; i<10; i++){
            armyTest.add(new RangedUnit("Ranger", 100));
        }
        armyTest.add(new CommanderUnit("Commander",100));
        assertEquals(1,armyTest.getCommanderUnits().size());
    }

    @Test
    void writeArmyToFileTest() {
        int i = 0;
        Army armyTest = new Army("Test army");
        for (i = 0; i<=100; i++){
            armyTest.add(new InfantryUnit("Footman", 100));
        }
        for (i = 0; i<=100; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        for (i = 0; i<100; i++){
            armyTest.add(new RangedUnit("Ranger", 100));
        }
        armyTest.add(new CommanderUnit("Commander",50000));

        armyTest.writeArmyToFile(armyTest.getAllUnits());
        System.out.println(armyTest.toString());
    }

    @Test
    void readArmyToFileTest() {
        int i = 0;
        Army armyTest = new Army("Test army2");
        for (i = 0; i<=100; i++){
            armyTest.add(new InfantryUnit("Footman", 100));
        }
        for (i = 0; i<=10; i++){
            armyTest.add(new CavalryUnit("Cavalry", 100));
        }
        for (i = 0; i<10; i++){
            armyTest.add(new RangedUnit("Ranger", 100));
        }
        armyTest.add(new CommanderUnit("Commander",50000));

        armyTest.writeArmyToFile(armyTest.getAllUnits());
        System.out.println(armyTest.toString());
    }


    @Test
    void writeAndReadArmyFromFileTest() {

    }

    @Test
    void getName() {
        Unit test = new InfantryUnit("Test",10);
        assertEquals(test.getName(),"Test");
    }

    @Test
    void add() {
        Army testArmy = new Army("Test army");
        testArmy.add(new InfantryUnit("test unit",10));
        assertEquals(1,testArmy.getAllUnits().size());
    }

    @Test
    void addAll() {
        ArrayList<Unit> units = new ArrayList<>();
        units.add(new InfantryUnit("test",1));
        units.add(new InfantryUnit("test",1));

        Army test = new Army("test");
        test.addAll(units);
        assertEquals(2,test.getAllUnits().size());
    }

    @Test
    void remove() {
        Army testArmy = new Army("Test army");
        Unit testUnit = new InfantryUnit("test unit",10);
        testArmy.add(testUnit);
        assertEquals(1,testArmy.getAllUnits().size());
        testArmy.remove(testUnit);
        assertEquals(0,testArmy.getAllUnits().size());
    }

    @Test
    void hasUnits() {
        Army testArmy = new Army("Test army");
        Unit testUnit = new InfantryUnit("test unit",10);
        testArmy.add(testUnit);
        assertTrue(testArmy.hasUnits());
    }

    @Test
    void getAllUnits() {
        Army test = new Army("test");
        for (int i = 0; i < 10 ; i++) {
            test.add(new InfantryUnit("test",100));
        }
        for (int i = 0; i < 10 ; i++) {
            test.add(new RangedUnit("test",100));
        }
        for (int i = 0; i < 10 ; i++) {
            test.add(new CavalryUnit("test",100));
        }
        test.add(new CommanderUnit("test",10));
        assertEquals(10,test.getInfantryUnits().size());
        assertEquals(10,test.getRangedUnits().size());
        assertEquals(11,test.getCavalryUnits().size());
        assertEquals(1, test.getCommanderUnits().size());
    }

    @Test
    void getRandom() {
        Army test = new Army("test");
        test.add(new InfantryUnit("test",1));
        test.add(new InfantryUnit("test",2));
        test.add(new InfantryUnit("test",3));
        System.out.println(test.getRandom().toString());
    }


}