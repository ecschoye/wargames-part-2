package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class WargamesClientTest
 * @author Edvard Schøyen
 *
 */
public class WargamesClientTest {

    @Test
    void main() {

    }

    @Test
    void readArmyFromFile() {
        //String resourcePath = "csv/Army.csv";

        String resourcePath = null;
        switch (System.getProperty("os.name")){
            case "Windows 10":
                resourcePath = "D:\\Wargames-part-2\\wargames-part-2\\Wargames\\csv\\ClientTest.csv";
            case "Mac OS X":
                resourcePath = "/Users/edvardschoyen/Documents/Kode/wargames-part-2/Wargames/csv/ClientTest.csv";
        }


        //String path = "D:\\Wargames-part-2\\wargames-part-2\\Wargames\\csv\\ClientTest.csv";
        Army armyFromFile = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(resourcePath))) {
            String line;
            String splitBy = ",";
            String armyName = reader.readLine();
            armyFromFile = new Army(armyName);
            while ((line = reader.readLine()) != null) {
                String[] units = line.split(splitBy);

                if (Objects.equals(units[0], "InfantryUnit")) {
                    armyFromFile.add(new InfantryUnit(units[1], Integer.parseInt(units[2])));
                } else if (Objects.equals(units[0], "CavalryUnit")) {
                    armyFromFile.add(new CavalryUnit(units[1], Integer.parseInt(units[2])));
                } else if (Objects.equals(units[0], "RangedUnit")) {
                    armyFromFile.add(new RangedUnit(units[1], Integer.parseInt(units[2])));
                } else if (Objects.equals(units[0], "CommanderUnit")) {
                    armyFromFile.add(new CommanderUnit(units[1], Integer.parseInt(units[2])));
                } else {
                    throw new IllegalArgumentException("An error occured while adding this unit. Please try again with a different unit.");
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Army testArmy = new Army("ClientTest");
        for (int i = 0; i <= 100; i++) {
            testArmy.add(new InfantryUnit("Footman", 100));
        }
        for (int i = 0; i <= 100 ; i++) {
            testArmy.add(new CavalryUnit("Cavalry", 100));
        }
        for (int i = 0; i < 100 ; i++) {
            testArmy.add(new RangedUnit("Ranger", 100));
        }
        testArmy.add(new CommanderUnit("Commander", 50000));

        assertEquals(armyFromFile, testArmy);
    }
}
