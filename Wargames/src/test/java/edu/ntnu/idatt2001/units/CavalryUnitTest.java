package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class CavalryUnitTest
 * @author Edvard Schøyen
 *
 */
public class CavalryUnitTest {
    /**
     * Checks that the attack bonus of the cavalry unit is 6
     */
    @Test
    void getAttackBonus() {
        Unit testUnit = new CavalryUnit("Raider", 100);
        assertEquals(6,testUnit.getAttackBonus());
    }

    /**
     * Checks that the resist bonus of the cavalry unit is 2
     */
    @Test
    void getResistBonus() {
        Unit testUnit = new CavalryUnit("Raider", 100);
        assertEquals(2,testUnit.getResistBonus());
    }

    /**
     * test to check cavalry units bonus damage
     * will have 6 attack bonus in the first attack, then 2 attack bonus in the later attacks
     */
    @Test
    public void bonusAttackDoesExtraDamage(){
        Unit testUnit = new CavalryUnit("Orc", 25);
        assertTrue(testUnit.getAttackBonus() == 6);
        assertTrue( testUnit.getAttackBonus() == 2);
        assertFalse( testUnit.getAttackBonus() == 6);
    }

    @Test
    void CavalryBonusDamageTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        assertEquals(6, testUnit2.getAttackBonus());
    }

    /**
     * Checks if cavalry unit's bonus damage is reduced from 6 to 2 after one attack
     */
    @Test
    void CavalryBonusDamageAfterOneAttackTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        assertEquals(2, testUnit1.getAttackBonus());
    }

    /**
     * Checks if cavalry unit's bonus damage is still 2 after two attacks
     */
    @Test
    void CavalryBonusDamageAfterTwoAttacksTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        testUnit1.attack(testUnit2);
        assertEquals(2, testUnit1.getAttackBonus());
    }

    @Test
    void CavalryBonusDamageAfterOneLookUpTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        System.out.println(testUnit1.getAttackBonus());
        assertEquals(2, testUnit1.getAttackBonus());
        System.out.println(testUnit1.getAttackBonus());
    }
}
