package edu.ntnu.idatt2001.units;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class RangedUnitTest
 * @author Edvard Schøyen
 *
 */
class RangedUnitTest {
    /**
     * Checks that the attack bonus of the ranged unit is 3
     */
    @Test
    void getAttackBonus() {
        Unit testUnit = new RangedUnit("Ranger", 100);
        assertEquals(3,testUnit.getAttackBonus());
    }
    /**
     * Checks that the resist bonus of the ranged unit is 6,
     * then it gets attacked once and checks if the resist bonsus is reduced to 4.
     * It is then attacked once again to see if the resist bonus is reduced to its minimum, 2.
     */
    @Test
    void getResistBonus() {
        Unit testUnit = new RangedUnit("Ranger", 100);
        Unit testUnit2 = new RangedUnit("Ranger", 100);
        assertEquals(6,testUnit.getResistBonus());
        testUnit2.attack(testUnit);
        assertEquals(4,testUnit.getResistBonus());
        testUnit2.attack(testUnit);
        assertEquals(2,testUnit.getResistBonus());
    }
}