package edu.ntnu.idatt2001.units;

import edu.ntnu.idatt2001.Army;
import edu.ntnu.idatt2001.Battle;
import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import org.junit.jupiter.api.Test;

/**
 * Class AttackDamageTest
 * @author Edvard Schøyen
 *
 */
public class AttackDamageTest {

    /**
     *
     */
    @Test
    void testIfUnitDoesDamageToOtherUnit(){
        Army testArmy1 = new Army("testArmy1");
        Army testArmy2 = new Army("testArmy2");
        testArmy1.add(new InfantryUnit("Footsoldier", 14));
        testArmy2.add(new CavalryUnit("Knight", 100));

        Battle testBattle = new Battle(testArmy1,testArmy2);
        testBattle.simulate();

        //assertEquals(testArmy1, testBattle.checkWhoWon(testArmy1, testArmy2));


    }
}
