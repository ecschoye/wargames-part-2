package edu.ntnu.idatt2001.units;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class InfantryUnitTest
 * @author Edvard Schøyen
 *
 */
class InfantryUnitTest {
    /**
     * Checks that the attack bonus of the infantry unit is 2
     */
    @Test
    void getAttackBonus() {
        Unit testUnit = new InfantryUnit("Infantry", 100);
        assertEquals(2,testUnit.getAttackBonus());
    }

    /**
     * Checks that the resist bonus of the infantry unit is 2
     */
    @Test
    void getResistBonus() {
        Unit testUnit = new InfantryUnit("Infantry", 100);
        assertEquals(2,testUnit.getResistBonus());
    }
}