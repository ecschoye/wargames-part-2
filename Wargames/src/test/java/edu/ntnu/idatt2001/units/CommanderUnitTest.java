package edu.ntnu.idatt2001.units;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class CommanderUnitTest
 * @author Edvard Schøyen
 *
 */
class CommanderUnitTest {
    /**
     * Checks that the attack bonus of the commander unit is 6
     */
    @Test
    void getAttackBonus() {
        Unit testUnit = new CommanderUnit("Commander", 100);
        assertEquals(6,testUnit.getAttackBonus());
    }

    /**
     * Checks that the resist bonus of the commander unit is 2
     */
    @Test
    void getResistBonus() {
        Unit testUnit = new CommanderUnit("Commander", 100);
        assertEquals(2,testUnit.getResistBonus());
    }
}