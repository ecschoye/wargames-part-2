package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.units.CavalryUnit;
import edu.ntnu.idatt2001.units.CommanderUnit;
import edu.ntnu.idatt2001.units.InfantryUnit;
import edu.ntnu.idatt2001.units.RangedUnit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;

/**
 * Class WargamesClient
 * @author Edvard Schøyen
 *
 */
public class WargamesClient {

    public static void main(String[] args) {
        int i;
        Army armyOne = new Army("Human Army");
        //Army armyTwo = new Army("Orcish Army");

        for (i = 0; i<=500; i++){
            armyOne.add(new InfantryUnit("Footman", 100));
            //armyTwo.add(new InfantryUnit("Grunt", 100));
        }

        for (i = 0; i<=100; i++){
            armyOne.add(new CavalryUnit("Knight", 100));
            //armyTwo.add(new CavalryUnit("Raider", 100));
        }
        for (i = 0; i<=200; i++){
            armyOne.add(new RangedUnit("Archer", 100));
            //armyTwo.add(new RangedUnit("Spearman", 100));
        }

        armyOne.add(new CommanderUnit("Mountain king", 180));
        //armyTwo.add(new CommanderUnit("Gul'dan", 180));
        //String resourcePath = "csv/Army.csv";

        String resourcePath = null;

        switch (System.getProperty("os.name")){
            case "Windows 10":
                resourcePath = "D:\\Wargames-part-2\\wargames-part-2\\Wargames\\csv\\Army.csv";

            case "Mac OS X":
                resourcePath = "/Users/edvardschoyen/Documents/Kode/wargames-part-2/Wargames/csv/Army.csv";
            default:
                System.out.println(System.getProperty("os.name"));
        }

        //String path = "D:\\Wargames-part-2\\wargames-part-2\\Wargames\\csv\\Army.csv";
        Army armyTwo = readArmyFromFile(resourcePath);
        Battle battle = new Battle(armyOne, armyTwo);
        battle.simulate();

    }

    public static Army readArmyFromFile(String pathToArmy){
        try (BufferedReader reader = new BufferedReader(new FileReader(pathToArmy))) {
            String line;
            String splitBy = ",";
            String armyName = reader.readLine();
            Army army = new Army(armyName);
            while ((line = reader.readLine()) != null){
                String[] units = line.split(splitBy);
                
                if (Objects.equals(units[0],"InfantryUnit")){
                    army.add(new InfantryUnit(units[1], Integer.parseInt(units[2])));
                }
                else if (Objects.equals(units[0],"CavalryUnit")){
                    army.add(new CavalryUnit(units[1], Integer.parseInt(units[2])));
                }
                else if (Objects.equals(units[0],"RangedUnit")){
                    army.add(new RangedUnit(units[1], Integer.parseInt(units[2])));
                }
                else if (Objects.equals(units[0],"CommanderUnit")){
                    army.add(new CommanderUnit(units[1], Integer.parseInt(units[2])));
                }
                else {
                    throw new IllegalArgumentException("An error occured while adding this unit. Please try again with a different unit.");
                }
            }
            reader.close();
            return army;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
