package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.units.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

/**
 * Class Army
 * @author Edvard Schøyen
 *
 */
public class Army {
    private final String name;
    private ArrayList<Unit> units = new ArrayList<Unit>();

    /**
     * Default constructor
     * @param name
     * @throws IllegalArgumentException
     */
    public Army(String name) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be blank.");
        this.name = name;
    }

    /**
     * Constructor. Takes in a list of units.
     * @param name
     * @param units
     * @throws IllegalArgumentException
     */
    public Army(String name, ArrayList<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be blank.");
        this.name = name;
        this.units = units;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a Unit to the list if the list does not contain the unit.
     * @param unit
     */
    public void add(Unit unit){
        if (!units.contains(unit)){
            units.add(unit);
        }
    }

    /**
     * Adds a list of Unit to the units list.
     * @param units
     */
    public void addAll(ArrayList<Unit> units){
        this.units.addAll(units);
    }

    /**
     * Removes a specific unit.
     * @param unit
     */
    public void remove(Unit unit){
            this.units.remove(unit);
    }

    /**
     * Checks if the list of units has Units
     * @return
     */
    public boolean hasUnits(){
        return !units.isEmpty();
    }

    /**
     *
     * @return units
     */
    public ArrayList<Unit> getAllUnits(){
        return this.units;
    }

    /**
     * Returns a random Unit from a list
     * If no Units in list, return null
     * @return Unit
     */
    public Unit getRandom(){
        if (hasUnits()){
            return this.units.get(new Random().nextInt(this.units.size()));
        }
        return null;
    }

    public ArrayList<Unit> getInfantryUnits(){
        ArrayList<Unit> infantryUnits = new ArrayList<Unit>();
        units.stream().filter(unit -> unit instanceof InfantryUnit).forEach(infantryUnits::add);
        return infantryUnits;
    }

    public ArrayList<Unit> getCavalryUnits(){
        ArrayList<Unit> cavalryUnits = new ArrayList<Unit>();
        units.stream().filter(unit -> unit instanceof CavalryUnit).forEach(cavalryUnits::add);
        units.stream().filter(unit -> unit instanceof CommanderUnit).forEach(cavalryUnits::remove);
        return cavalryUnits;
    }

    public ArrayList<Unit> getRangedUnits(){
        ArrayList<Unit> rangedUnits = new ArrayList<Unit>();
        units.stream().filter(unit -> unit instanceof RangedUnit).forEach(rangedUnits::add);
        return rangedUnits;
    }

    public ArrayList<Unit> getCommanderUnits(){
        ArrayList<Unit> commanderUnits = new ArrayList<Unit>();
        units.stream().filter(unit -> unit instanceof CommanderUnit).forEach(commanderUnits::add);
        return commanderUnits;
    }

    public String arrayListToString(ArrayList<Unit> list){
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach(stringBuilder::append);
        return stringBuilder.toString();
    }

    public void writeArmyToFile(ArrayList<Unit> units){
        String resourcePath = null;

        switch (System.getProperty("os.name")){
            case "Windows 10":
                resourcePath = "D:\\Wargames-part-2\\wargames-part-2\\Wargames\\csv\\Army.csv";

            case "Mac OS X":
                resourcePath = "/Users/edvardschoyen/Documents/Kode/wargames-part-2/Wargames/csv/Army.csv";
            default:
                System.out.println(System.getProperty("os.name"));
        }

        try (FileWriter fileWriter = new FileWriter(resourcePath)){
            fileWriter.write(this.name + "\n");
            for (Unit unit : units) {
                fileWriter.write(unit.convertToCsvFormat());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder results = new StringBuilder(": ");
        //units.stream().filter(unit -> unit instanceof Unit).forEach(Unit::toString);
        for (Unit unit : units){
            results.append("\n").append(unit.getClass().getSimpleName()).append(" ").append(unit.getName()).append(" with ").append(unit.getHealth()).append("hp");
        }
        return "The army, " + name + ", with remaining units " + results;
    }


    /**
     * Checks if two objects are equals
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
